'''
DAY1
We can assume that patient data is independent from another and that the initial time a patient is submitted to the hospital does not matter.
Since it is time series data, we should use a time-series data imputation technique. Does the data show strong seasonality? No.
It might be fine to use linear interpolation. Higher smoothness would not be overtly beneficial since we only have 12 time points.
If for a specific feature and patient, no data is available, use the global mean value of this feature. If the quality is poor we could
consider k-NN-based techniques to create more believable values. 

DAY2
Separate SVMs for Task 1 and 2, such as linear regression for task 3 seems to pass the easy baseline. Obviously, the coolest thing 
to have here is a CNN for time series data, it is the most powerful model for this type of problem that we have seen in this lecture (yet).

DAY3
We tried to use tensorflow for a time-series CNN, but there were some compatibility issues we were not able to resolve, so we rolled back to 
traditional svm and linear regression. First tests with a 4-layer perceptron showed promising results, as it could perfectly fit the training 
data (sufficient representation power). Cross-validation should be able to produce decent predictors. Keras might be a good thing to look into for giving time-series 
CNNs another try: https://towardsdatascience.com/building-a-convolutional-neural-network-cnn-in-keras-329fbbadc5f5

DAY4
Replaced the first 10 classifiers by MLPClassifiers with high tolerance to prevent overfitting and achieved .73. Interestingly,
we did not find a better regressor than the unregularized linear one on handpicked features.

'''

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn import neural_network
from sklearn import linear_model
import sklearn.metrics as metrics
import seaborn as sns

VITALS = ['LABEL_RRate', 'LABEL_ABPm', 'LABEL_SpO2', 'LABEL_Heartrate']
TESTS = ['LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
         'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2',
         'LABEL_Bilirubin_direct', 'LABEL_EtCO2']


def plot_patient( data, pn, title = "" ):
	
	patient = data[ 12*pn:12*pn+12 ]
	x = np.arange( 12 )
	(_,cols) = data.shape

	#includes age
	for col in range( 2, cols ):
		
		y = patient[ :, col ]
		plt.plot( x, y )
		
	plt.title( "patient " + str( pn ) +" "+ title )
	plt.show( )

def local_impute_series( series ):
	
	pd_series = pd.Series( series, copy = False )
	pd_series.interpolate( method = 'linear', limit_direction = 'both', inplace = True )

	return pd_series.to_numpy( copy = False )

def local_impute_patient( data, pn ):

	patient = data[ 12*pn:12*pn+12 ]
	(_,cols) = data.shape

	for col in range( 3, cols ):
		
		series = patient[ :, col ]

		all_nan = True
		for v in series:
			if not math.isnan( v ): all_nan = False

		if all_nan: continue #cannot do local imputation, fallback to global imputation

		patient[ :, col ] = local_impute_series( series )

	mini = patient[ 0, 1 ]
	maxi = patient[ 11, 1 ]

	if maxi > 24:
		patient[ :, 1 ] = np.arange( 1, 13 )

def global_impute_patients( data ):

	df = pd.DataFrame( data )

	(_,cols) = data.shape
	for col in range( cols ):

		df.iloc[ :, col ] = df.iloc[ :, col ].fillna( np.nanmean( data[ :, col ] ))

	data = df.to_numpy( )

def impute( data, save = False, name = "" ):

	( n, cols ) = data.shape
	patient_count = int( n / 12 )

	for pn in range( patient_count ):
		
		local_impute_patient( data, pn )
		show_progress( "Imputation", progress = ( pn + 1 )/ patient_count )

	end_show_progress( )
	global_impute_patients( data )

	if save :
		out = pd.DataFrame( data )
		out.to_csv( name, index = False, header = False, float_format = "%.3f" )		

def get_X_mean( features ):

	(n, cols) = features.shape
	patient_count = int(n/12)

	use_cols = range( 2, cols )
	X = np.zeros( ( patient_count, len( use_cols ))) #exclude pid and time

	for pn in range( patient_count ):

		patient = features[ 12*pn:12*pn+12, : ]
		means = [ np.mean( patient[ :, col ]) for col in use_cols ]
		X[ pn, : ] = means

	return X

def get_X_handpicked( features ):

	(n, cols) = features.shape
	patient_count = int(n/12)

	use_cols = range( 2, cols )
	X = np.zeros( ( patient_count, len( use_cols ) * 3 )) #exclude pid and time use first,last and mean

	for pn in range( patient_count ):

		patient = features[ 12*pn:12*pn+12, : ]
		means = [ np.mean( patient[ :, col ]) for col in use_cols ]
		firsts = [ patient[ 0, col ] for col in use_cols ]
		lasts = [ patient[ -1, col ] for col in use_cols ]
		X[ pn, : ] = means + firsts + lasts

	return X	

def get_X_handpicked_augmented( features ):

	(n, cols) = features.shape
	patient_count = int(n/12)

	use_cols = range( 2, cols )
	X = np.zeros( ( patient_count, len( use_cols ) * 3 + 2 )) #exclude pid and use first,last and mean

	for pn in range( patient_count ):

		patient = features[ 12*pn:12*pn+12, : ]
		time_range = [ patient[ 0, 1 ], patient[ 11, 1 ]]
		means = [ np.mean( patient[ :, col ]) for col in use_cols ]
		firsts = [ patient[ 0, col ] for col in use_cols ]
		lasts = [ patient[ -1, col ] for col in use_cols ]
		X[ pn, : ] = time_range + means + firsts + lasts

	return X	

def get_X_full( features ):

	(n, cols) = features.shape
	patient_count = int(n/12)

	use_cols = range( 2, cols ) #exclude pid and time
	X = np.zeros( ( patient_count, len( use_cols ) * 12 )) 

	for pn in range( patient_count ):

		patient = features[ 12*pn:12*pn+12, : ]
		flat = [ ]
		
		for t in range( 12 ):

			flat = np.append( flat, patient[ t, use_cols ], axis = 0 )

		X[ pn, : ] = flat

	return X

def get_X_full_augmented( features ):

	(n, cols) = features.shape
	patient_count = int(n/12)

	use_cols = range( 2, cols ) #exclude pid and time
	X = np.zeros( ( patient_count, len( use_cols ) * 12 + 2 )) 

	for pn in range( patient_count ):

		patient = features[ 12*pn:12*pn+12, : ]
		flat = [ patient[ 0, 1 ], patient[ 1, 1 ]]
		
		for t in range( 12 ):

			flat = np.append( flat, patient[ t, use_cols ], axis = 0 )

		X[ pn, : ] = flat

	return X

def standardize( X ):

	return ( X - np.mean( X, axis = 0 ))/ np.std( X, axis = 0 )

def get_X1( features ):

	return standardize( get_X_full( features ))
	#return get_X_handpicked( features )

def get_X2( features ):

	return standardize( get_X_full( features ))

def get_X3( features ):

	#return standardize( get_X_full_augmented( features ))
	#return get_X_handpicked( features )
	return get_X_handpicked_augmented( features )

def get_y2( labels ):

	y2 = labels.to_numpy( )[ :, 11 ]

	return y2

def get_y1( labels ):

	y1 = labels.to_numpy( )[ :, 1:11 ]

	return y1

def get_y3( labels ):

	y3 = labels.to_numpy( )[ :, 12: ]

	return y3

def show_progress( name, progress, bars = 50 ):
	
	print( name + " " + int(progress*bars) * '█' + int((1-progress)*bars) * '-' + " " + str(int(progress*100)) + "%", end = '\r' )

def end_show_progress( ):

	print( )

#numpy 1d array or pd.Series
def fitness_classify( true, submission ):

	return metrics.roc_auc_score( true, submission )

def split_1d( X, frac ):

	(n,) = X.shape
	b = int( n * frac )	
	return ( X[ :b ], X[ b: ])

def split_2d( X, frac ):

	(n,_) = X.shape
	b = int( n * frac )	
	return ( X[ :b, : ], X[ b:, : ])

def shuffle_seeded( arr, state ):

	np.random.set_state( state )
	np.random.shuffle( arr )

def shuffle_pair( X, y ):

	s = np.random.get_state( )
	shuffle_seeded( X, s )
	shuffle_seeded( y, s )

def cross_validate( X, y, params, model_f, score_f ):

	bestparam = 0
	bestmodel = None
	bestscore = - math.inf

	for p in params:

		model = model_f( p, X, y )
		score = score_f( model, X, y )

		print( "Param {0} scores {1}".format( p, score ))

		if score > bestscore:

			bestscore = score
			bestparam = p
			bestmodel = model

	print( "Best param was {0} with score {1}".format( bestparam, bestscore ))
	return ( bestparam, bestscore, bestmodel )

def task2_model_f( alpha, X, y ):

	( X_train, _ ) = split_2d( X, .8 )
	( y_train, _ ) = split_1d( y, .8 )

	model = neural_network.MLPClassifier( hidden_layer_sizes = ( 100, 100 ), alpha = alpha )
	model.fit( X_train, y_train )

	return model

def task2_score_f( model, X, y ):

	( _, X_cv ) = split_2d( X, .8 )
	( _, y_cv ) = split_1d( y, .8 )

	y_hat = model.predict_proba( X_cv )[ :, 1 ]
	score = fitness_classify( y_cv, y_hat )

	return score

class Model:

	def fit( self, features, labels ):
		
		# task 1 ----------------------------------------------------

		show_progress( "Task 1 (Preprocessing)", 0 )

		X1 = get_X1( features )
		y1 = get_y1( labels )

		show_progress( "Task 1 (Training)", 0 )

		alpha = 1
		#models1 = [ svm.LinearSVC( tol = 1e-4, dual = False, class_weight = 'balanced', loss = 'squared_hinge', C = 2 ) for test in TESTS ]
		models1 = [ neural_network.MLPClassifier( tol = 1, hidden_layer_sizes = ( 100, 100 ), alpha = alpha ) for test in TESTS ]
		
		for i in range(len( models1 )):

			models1[ i ].fit( X1, y1[ :, i ])
			show_progress( "Task 1 (Training)", (i+1)/ len( models1 ))
		
		end_show_progress( )
		
		self.models1 = models1

		# task 2 ----------------------------------------------------

		show_progress( "Task 2 (Preprocessing)", 0 )

		X2 = get_X2( features )
		y2 = get_y2( labels )

		show_progress( "Task 2 (Training)", .5 )

		alphas = [ 1e-7, 1e-5, 1e-3, 1e-1, 1, 1e1, 1e3 ]

		model_f = lambda alpha, X, y : task2_model_f( alpha, X, y ) 
		score_f = lambda model, X, y : task2_score_f( model, X, y )

		alpha = 1
		#shuffle_pair( X2, y2 )
		#( alpha, score, _ ) = cross_validate( X2, y2, alphas, model_f, score_f )

		model2 = neural_network.MLPClassifier( tol = 1, hidden_layer_sizes = ( 100, 100 ), alpha = alpha )
		model2.fit( X2, y2 )

		self.model2 = model2

		show_progress( "Task 2 (Training)", 1 )
		end_show_progress( )

		# task 3 ----------------------------------------------------		

		show_progress( "Task 3 (Preprocessing)", 0 )

		X3 = get_X3( features )
		y3 = get_y3( labels )

		show_progress( "Task 3 (Training)", .5 )

		#alpha = 1
		#model3 = neural_network.MLPRegressor( hidden_layer_sizes = ( 500, 100 ), alpha = alpha )
		
		model3 = linear_model.LinearRegression( )

		model3.fit( X3, y3 )

		self.model3 = model3

		show_progress( "Task 3 (Training)", 1 )
		end_show_progress( )
		
	def predict( self, features, submit = False ):

		(n, cols) = features.shape
		patient_count = int(n/12)
		columns = ['pid'] + TESTS + ['LABEL_Sepsis'] + VITALS
		df = pd.DataFrame( data = np.zeros(( patient_count, len( columns ))), columns = columns )
		df.iloc[ :, 0 ] = pd.Series( features[ ::12, 0 ])

		# task 1 ----------------------------------------------------

		X1 = get_X1( features )

		for i, test in enumerate( TESTS ):

			df.loc[ :, test ] = pd.Series( self.models1[ i ].predict_proba( X1 )[ :, 1 ]) ##.decision_function( X1 ) for svm

		# task 2 ----------------------------------------------------

		X2 = get_X2( features )

		df.loc[ :, 'LABEL_Sepsis' ] = pd.Series( self.model2.predict_proba( X2 )[ :, 1 ])

		# task 3 ----------------------------------------------------	

		X3 = get_X3( features )

		df.iloc[ :, 12: ] = self.model3.predict( X3 )

		# end    ----------------------------------------------------	

		if submit:
			df.to_csv( "prediction.zip", index = False, header = True, float_format="%.3f", compression = 'zip' )

		else:
			df.to_csv( "prediction.csv", index = False, header = True, float_format="%.3f" )

		return df

	def fitness( df_true, df_submission ):
	    
	    df_submission = df_submission.sort_values('pid')
	    df_true = df_true.sort_values('pid')
	    task1 = np.mean([metrics.roc_auc_score(df_true[entry], df_submission[entry]) for entry in TESTS])
	    task2 = metrics.roc_auc_score(df_true['LABEL_Sepsis'], df_submission['LABEL_Sepsis'])
	    task3 = np.mean([0.5 + 0.5 * np.maximum(0, metrics.r2_score(df_true[entry], df_submission[entry])) for entry in VITALS])
	    score = np.mean([task1, task2, task3])
	    print(task1, task2, task3)
	    return score

def main( submit = False ):

	np.random.seed( 0 )

	features = pd.read_csv( "train_features_imputed.csv", header = None ).to_numpy( )
	task_features = pd.read_csv( "test_features_imputed.csv", header = None ).to_numpy( )
	labels = pd.read_csv( "train_labels.csv" )
	( n, cols ) = features.shape
	patient_count = int( n / 12 )

	test_begin = .8
	train_end = .8

	fe = int( patient_count * train_end ) * 12
	fb = int( patient_count * test_begin ) * 12
	features_train = features[ :fe, : ]
	features_test = features[ fb:, : ]

	le = int( patient_count * train_end )
	lb = int( patient_count * test_begin )
	labels_train = labels.iloc[ :le, : ]
	labels_test = labels.iloc[ lb:, : ]

	m = Model( )
	
	if submit:
		m.fit( features, labels )
		m.predict( task_features, submit = True )
		print( "Done with submission" )

	else:
		m.fit( features_train, labels_train )
		print( "Train set" )
		labels_predicted = m.predict( features_train )
		print( Model.fitness( labels_train, labels_predicted ))
		
		print( "Test set" )
		labels_predicted = m.predict( features_test )
		print( Model.fitness( labels_test, labels_predicted ))

def zip_solution( ):

	df = pd.read_csv( "prediction.csv" )
	df.to_csv( "prediction.zip", index = False, header = True, float_format="%.3f", compression = 'zip' )

def time_analysis( ):

	data = pd.read_csv( "train_features.csv" ).to_numpy( )
	
	( n, cols ) = data.shape
	patient_count = int( n / 12 )

	for pn in range( patient_count ):

		patient = data[ 12*pn:12*pn+12 ]

		mini = patient[ 0, 1 ]
		maxi = patient[ 11, 1 ]

		if maxi > 24:
 	 		print( "Range {0} to {1} ".format( mini, maxi ))

def get_visualized_X( features ):

	(n, cols) = features.shape
	patient_count = int(n/12)

	use_cols = range( 2, cols ) #exclude pid and time
	X = np.zeros( ( patient_count, 2 )) 

	for pn in range( patient_count ):

		patient = features[ 12*pn:12*pn+12, : ]

		X[ pn, : ] = [ np.mean( patient[ :, use_cols[ 1 ]]) ] + [ np.mean( patient[ :, use_cols[ 1 ]]) ]

	return X

def get_visualized_y( labels ):

	y2 = labels.to_numpy( )[ :, 1 ]

	return y2

def visualize( ):

	features = pd.read_csv( "train_features_imputed.csv", header = None ).to_numpy( )
	labels = pd.read_csv( "train_labels.csv" )

	X = get_visualized_X( features )
	y = np.expand_dims( get_visualized_y( labels ), axis = 1 )

	d = np.concatenate( ( X, y ), axis = 1 )
	data = pd.DataFrame( d, columns = [ "x1", "x2", "label" ])
	
	sns.relplot( x = "x1", y = "x2", hue = "label", data = data )
	plt.show( )
 
if __name__ == '__main__':
	
	visualize( )

	#time_analysis( )

	#print( pd.__version__ )
	#data = pd.read_csv( "test_features_imputed.csv" ).to_numpy( )
	#plot_patient( data, 0, "fabian patient" )

	#impute( pd.read_csv( "test_features.csv" ).to_numpy( ), save = True, name = "test_features_imputed.csv" )
	#impute( pd.read_csv( "train_features.csv" ).to_numpy( ), save = True, name = "train_features_imputed.csv" )

	# dmain( submit = True )
