import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV
def preprocessing( train, cols ):
	
	names = train[ "Sequence" ]
	( n, _ ) = train.shape

	df = pd.DataFrame( 0., index = np.arange( n ), columns = cols )
	counter = 0
	for name in names:
		for i in range( 4 ):
			ch = name[ i ]
			df.at[ counter, ch + str( i ) ] = 1.
		counter = counter + 1

	return df

def get_X( features ):

	alphabet = "RHKDESTNQCGPAILMFWYV"
	cols = []

	for i in range( 4 ):
		for char in alphabet:
			cols.append( char + str( i ))

	X = preprocessing( features, cols )
	X = X.to_numpy( )

	return X

if __name__ == '__main__':
	
	np.random.seed( 1 )

	train = pd.read_csv( "train.csv" )
	y = train[ "Active" ].to_numpy( )
	X = get_X( train )
	X = (X-X.mean(axis = 0))/X.std(axis = 0) #normalize X to zero mean and unit variance
	#________Parameter Search___________
	param_test2 = {'hidden_layer_sizes': [(100,),(100,100),(100,100,100),(100,100,100,100),(100,100,100,100,100)],
					'activation': ['identity','logistic','tanh','relu'], 'alpha' : [0.00001, 0.0001, 0.001, 0.01, 0.1, 1,10,100]}	
	print("_____Start Parameter Search_______")
	gsearch2 = GridSearchCV(estimator = MLPClassifier(),
   					param_grid = param_test2, scoring='f1',cv=5,n_jobs = -1)
	gsearch2.fit(X,y)
	print("_____Parameter Search Finished______")
	print(gsearch2.best_score_)
	print(gsearch2.best_params_)

	#________FIT_________________
	'''
	X, y = shuffle( X, y )
	X_train, X_test, y_train, y_test = train_test_split( X, y, test_size = 0.2 )

	model = MLPClassifier( hidden_layer_sizes = ( 100, 100 ), tol = .1 )
	model.fit( X_train, y_train )

	y_predict = model.predict( X_test )
	print( metrics.f1_score( y_test, y_predict ))
	'''

	#________PREDICT_____________
	'''
	task = pd.read_csv( "test.csv" )
	X_task = get_X( task )
	y_task = model.predict( X_task )
	pd.DataFrame( y_task ).to_csv( "result.csv", index = False, header = False )
	'''
