#import os
#os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"

import keras
import pandas as pd
import numpy as np
import sklearn
from keras import Sequential
from keras.layers import Dense, BatchNormalization, Activation, Dropout
from sklearn.feature_selection import SelectKBest
import tensorflow as tf
import keras.backend as K
import math
from keras import regularizers
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

from sklearn.metrics import f1_score
from numpy.random import seed

NUM_LETTERS = 21
NUM_LOCATIONS = 4
NUM_FEATURES = NUM_LETTERS * NUM_LOCATIONS
RANDOM_SEED = 1
seed(RANDOM_SEED)

np.random.seed(RANDOM_SEED)
tf.random.set_seed(RANDOM_SEED)

def f1_loss(y_true, y_pred):
    
    tp = K.sum(K.cast(y_true*y_pred, 'float'), axis=0)
    tn = K.sum(K.cast((1-y_true)*(1-y_pred), 'float'), axis=0)
    fp = K.sum(K.cast((1-y_true)*y_pred, 'float'), axis=0)
    fn = K.sum(K.cast(y_true*(1-y_pred), 'float'), axis=0)

    p = tp / (tp + fp + K.epsilon())
    r = tp / (tp + fn + K.epsilon())

    f1 = 2*p*r / (p+r+K.epsilon())
    f1 = tf.where(tf.math.is_nan(f1), tf.zeros_like(f1), f1)
    return 1 - K.mean(f1)

def decode_letter(letter):
	switcher = {
		'R': 0,
		'H': 1,
		'K': 2,
		'D': 3,
		'E': 4,
		'S': 5,
		'T': 6,
		'N': 7,
		'Q': 8,
		'C': 9,
		'U': 10,
		'G': 11,
		'P': 12,
		'A': 13,
		'I': 14,
		'L': 15,
		'M': 16,
		'F': 17,
		'W': 18,
		'Y': 19,
		'V': 20
	}
	return switcher.get(letter,"Invalid letter")

def data_preparation():

	print('Preparing training data')

	# Prepare training data

	df = pd.read_csv("train.csv")
	num_rows = df.shape[0]

	df_converted = np.empty([num_rows,NUM_FEATURES + 1])

	for index, row in df.iterrows():
		df_converted[index][NUM_FEATURES] = row[1]
		letters = row[0]
		for i in range(len(letters)):
			df_converted[index][(decode_letter(letters[i])) + (NUM_LETTERS * i)] = 1

	pd.DataFrame(df_converted.astype(int)).to_csv("train_converted.csv")

	# Prepare test data

	print('Preparing testing data')

	df = pd.read_csv("test.csv")
	num_rows = df.shape[0]

	df_converted1 = np.empty([num_rows,NUM_FEATURES])

	for index, row in df.iterrows():
		letters = row[0]
		for i in range(len(letters)):
			df_converted1[index][(decode_letter(letters[i])) + (NUM_LETTERS * i)] = 1

	pd.DataFrame(df_converted1.astype(int)).to_csv("test_converted.csv")

	return df_converted, df_converted1

def downsample(df):
	shuffled_df = df.sample(frac=1, random_state=RANDOM_SEED)
	active_df = shuffled_df.loc[shuffled_df['{}'.format(NUM_FEATURES)] == 1]
	inactive_df = shuffled_df.loc[shuffled_df['{}'.format(NUM_FEATURES)] == 0].sample(n=active_df.shape[0],random_state=RANDOM_SEED)
	df = pd.concat([active_df, inactive_df])
	return df.sample(frac=1, random_state=RANDOM_SEED)	

def exp_decay(epoch):
   initial_lrate = 0.1
   k = 0.1
   lrate = initial_lrate * exp(-k*t)
   return lrate

def main():
	df = pd.read_csv("train_converted.csv")	
	df1 = pd.read_csv("test_converted.csv")

	#df = downsample(df)

	X_train = df.iloc[:,0:NUM_FEATURES].to_numpy()
	X_test = df1.iloc[:,0:NUM_FEATURES].to_numpy()

	y_train = df.iloc[:,NUM_FEATURES+1].to_numpy()

	scaler = StandardScaler().fit(X_train)
	X_train = scaler.transform(X_train)
	X_test = scaler.transform(X_test)

	counts = np.bincount(y_train)
	num_neg = counts[0]
	num_pos = counts[1]
	num_all = num_neg + num_pos
	ratio_neg_to_pos = num_neg / num_pos
	print('Ratio negatives:positives {}:1'.format(ratio_neg_to_pos))

	weight_for_neg = 1
	weight_for_pos = ratio_neg_to_pos
	class_weight = {0:weight_for_neg, 1:weight_for_pos}

	print('Using this weight for negatives: {}'.format(weight_for_neg))
	print('Using this weight for positives: {}'.format(weight_for_pos))

	try_num_best_features = [NUM_FEATURES]
	try_neurons_per_layer = [8, 16, 64, 100, 200, 300, 400, 450, 475, 500, 525, 550, 600, 650]
	try_activation_1 = ['relu']
	try_activation_2 = ['tanh']
	try_activation_3 = ['relu']
	try_dropout = [0.05, 0.075, 0.1, 0.125, 0.15, 0.2, 0.25, 0.3, 0.4]

	best_f1_score = 0.0
	best_num_best_features = 0
	best_neurons_per_layer = 0
	best_activation_1 = ''
	best_activation_2 = ''
	best_activation_3 = ''
	best_dropout = 0.0
	best_config_nr = 0

	start_from = 0
	config_nr = 0

	for num_best_features in try_num_best_features:
		for neurons_per_layer in try_neurons_per_layer:
			for activation_1 in try_activation_1:
				for activation_2 in try_activation_2:
					for activation_3 in try_activation_3:
						for dropout in try_dropout:

							config_nr += 1

							if config_nr < start_from:
								continue

							'''
							selector = SelectKBest(k=num_best_features)
							selector.fit(X_train,y_train)
							X_train = selector.transform(X_train)
							X_test = selector.transform(X_test)
							'''

							y_train_categorical = keras.utils.to_categorical(y_train)

							model = Sequential([

								Dense(neurons_per_layer, input_dim=num_best_features, kernel_initializer='uniform', activation=activation_1),
								BatchNormalization(),
								Dropout(dropout),

								Dense(neurons_per_layer, kernel_initializer='uniform', activation=activation_2),
								BatchNormalization(),
								Dropout(dropout),

								Dense(neurons_per_layer, kernel_initializer='uniform', activation=activation_3),
								BatchNormalization(),
								Dropout(dropout),

								Dense(2, kernel_initializer='uniform',activation='softmax'),
								BatchNormalization()
							])

							model.summary()
						
							sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)

							metrics = [
							    keras.metrics.FalseNegatives(name="fn"),
							    keras.metrics.FalsePositives(name="fp"),
							    keras.metrics.TrueNegatives(name="tn"),
							    keras.metrics.TruePositives(name="tp"),
							    keras.metrics.Precision(name="p"),
							    keras.metrics.Recall(name="r")
							]	

							model.compile(loss='binary_crossentropy', optimizer=sgd, metrics=metrics)

							hist = model.fit(X_train, y_train_categorical, validation_split=0.1, epochs=100, batch_size=512, class_weight=class_weight)

							val_p = hist.history['val_p'][-1]
							val_r = hist.history['val_r'][-1]
							if not (val_p == 0.0 or val_r == 0.0):
								val_f1 = 2 * val_p * val_r / (val_p + val_r)
							else:
								val_f1 = 0

							if val_f1 > best_f1_score:
								best_f1_score = val_f1
								best_num_best_features = num_best_features
								best_neurons_per_layer = neurons_per_layer
								best_activation_1 = activation_1
								best_activation_2 = activation_2
								best_activation_3 = activation_3
								best_dropout = dropout
								best_config_nr = config_nr

								model.save('best_model.h5')

								print('\n ********* New max f1: {}, config_nr: {} *********'.format(best_f1_score,best_config_nr))
								print('features: {},\nneurons per layer: {}\nactivation 1: {}\n activation 2: {}\n activation 3: {}\n dropout: {}'.format(best_num_best_features,best_neurons_per_layer,best_activation_1,best_activation_2,best_activation_3,best_dropout))

								y_test = model.predict_classes(X_test)
								pd.DataFrame(y_test).to_csv('result.csv', header=False, index=False)

								y_test = model.predict(X_test)
								pd.DataFrame(y_test).to_csv('result_1.csv', header=False, index=False)
								y_test_1 = np.argmax(y_test, axis=1)
								pd.DataFrame(y_test_1).to_csv('result_2.csv', header=False, index=False)

							else:
								print('\n --------- Config Nr. {} --- No new max f1. Best until now: f1: {} with config_nr: {} ---------'.format(config_nr, best_f1_score,best_config_nr))

if __name__ == '__main__':
	# data_preparation()
	main()

	