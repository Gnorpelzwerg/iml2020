'''
Recall and precision are important. F1 score is used

AA = amino acid

# Option 0

- 4 Input neurons, one per possible AA position
- Then encode letters as numbers (1 through 26)

Might be problematic because it suggests some letters are more similar to one another than others.
I.e. A and B would have distance 1 whereas A and B would have distance 25. This doesn't reflect
reality since the letters are a abstraction for AAs.


Option 1:
26 letters x 4 locations = 104 input neurons to cover every configuration of AAs.
Input = 1 if there is a specific letter at a specific location, 0 otherwise.



'''

import simpleaudio as sa

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn

from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score # Usage: f1_score(y_true, y_predicted)
from sklearn.utils import class_weight
from math import sqrt

import tensorflow as tf
import keras
import keras.backend as K
# from keras.modules import Sequential
from keras.layers import Dense
from keras.layers import BatchNormalization
from keras.layers import Dropout
from keras.utils import to_categorical

NUM_LETTERS = 21

RANDOM_SEED = 42

np.random.seed(RANDOM_SEED)
tf.random.set_seed(RANDOM_SEED)

def decode_letter(letter):
	switcher = {
		'R': 0,
		'H': 1,
		'K': 2,
		'D': 3,
		'E': 4,
		'S': 5,
		'T': 6,
		'N': 7,
		'Q': 8,
		'C': 9,
		'U': 10,
		'G': 11,
		'P': 12,
		'A': 13,
		'I': 14,
		'L': 15,
		'M': 16,
		'F': 17,
		'W': 18,
		'Y': 19,
		'V': 20
	}
	return switcher.get(letter,"Invalid letter")


def convert_training_data():
	df = pd.read_csv("train.csv")

	converted_data = np.empty([df.shape[0],4 * NUM_LETTERS + 1])

	for index, row in df.iterrows():
		letters = row[0]
		converted_data[index][4 * NUM_LETTERS] = row[1]
		for i in range(len(letters)):
			converted_data[index][(decode_letter(letters[i])) + (NUM_LETTERS * i)] = 1

	pd.DataFrame(converted_data.astype(int)).to_csv("train_converted.csv")

def convert_test_data():
	df = pd.read_csv("test.csv")

	converted_data = np.empty([df.shape[0],4 * NUM_LETTERS + 1])

	for index, row in df.iterrows():
		letters = row[0]
		for i in range(len(letters)):
			converted_data[index][(decode_letter(letters[i])) + (NUM_LETTERS * i)] = 1

	pd.DataFrame(converted_data.astype(int)).to_csv("test_converted.csv")

def my_f1(precision,recall):
	return 2 * (precision * recall) / (precision + recall)

import tensorflow as tf

def f1(y_true, y_pred):
    y_pred = K.round(y_pred)
    tp = K.sum(K.cast(y_true*y_pred, 'float'), axis=0)
    tn = K.sum(K.cast((1-y_true)*(1-y_pred), 'float'), axis=0)
    fp = K.sum(K.cast((1-y_true)*y_pred, 'float'), axis=0)
    fn = K.sum(K.cast(y_true*(1-y_pred), 'float'), axis=0)

    p = tp / (tp + fp + K.epsilon())
    r = tp / (tp + fn + K.epsilon())

    f1 = 2*p*r / (p+r+K.epsilon())
    f1 = tf.where(tf.math.is_nan(f1), tf.zeros_like(f1), f1)
    return K.mean(f1)

def f1_loss(y_true, y_pred):
    
    tp = K.sum(K.cast(y_true*y_pred, 'float'), axis=0)
    tn = K.sum(K.cast((1-y_true)*(1-y_pred), 'float'), axis=0)
    fp = K.sum(K.cast((1-y_true)*y_pred, 'float'), axis=0)
    fn = K.sum(K.cast(y_true*(1-y_pred), 'float'), axis=0)

    p = tp / (tp + fp + K.epsilon())
    r = tp / (tp + fn + K.epsilon())

    f1 = 2*p*r / (p+r+K.epsilon())
    f1 = tf.where(tf.math.is_nan(f1), tf.zeros_like(f1), f1)
    return 1 - K.mean(f1)

def main():
	df = pd.read_csv("train_converted.csv")
	X = df.iloc[:,0:4 * NUM_LETTERS].values
	y = df.iloc[:,4 * NUM_LETTERS + 1].values

	X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.30, random_state=RANDOM_SEED)

	class_weights = class_weight.compute_class_weight('balanced',
                                         			np.unique(y_train),
                                         			y_train)
	class_weight_dict = dict(enumerate(class_weights))

	print(class_weights)

	y_test_og = y_test

	#y_train = to_categorical(y_train)
	#y_test = to_categorical(y_test)

	weights = {0:1, 1:26}

	model = keras.Sequential()
	model.add(Dense(10, input_dim=4*NUM_LETTERS, activation='relu', kernel_initializer='he_uniform'))
	model.add(BatchNormalization())
	model.add(Dropout(0.25))
	model.add(Dense(1, activation='sigmoid'))

	model.compile(loss='binary_crossentropy', optimizer='SGD', metrics=[keras.metrics.Precision(),keras.metrics.Recall(),f1])

	model.fit(X_train, y_train, epochs=1, class_weight=weights, validation_split=0.05, shuffle=True)

	pred_test= model.predict(X_test)
	scores2 = model.evaluate(X_test, y_test, verbose=0)

	precision = scores2[0]
	recall = scores2[1]

	print(pred_test)

	threshold = 0
	step_size = 0.0001

	pred_test_og = pred_test

	best_f1 = 0
	best_threshold = 0

	while threshold < 1:
		print("Current threshold: {}".format(threshold))
		threshold += step_size
		for i in range(len(pred_test_og)):
			if pred_test_og[i] > threshold:
				pred_test[i] = 1

		this_f1 = f1_score(y_test,pred_test)
		if this_f1 > best_f1:
			best_f1 = this_f1
			best_threshold = threshold

	print(best_f1)
	print(best_threshold)

	print('Precision: {} \nRecall: {}\nF1: {}'.format(scores2[0], scores2[1], best_f1)) 


def data_exploration():
	df = pd.read_csv("train_converted.csv")
	y = df.iloc[:,4 * NUM_LETTERS + 1].values

	count_all = 0
	count_pos = 0
	count_neg = 0
	for i in y:
		count_all += 1
		if i == 1:
			count_pos += 1
		else:
			count_neg += 1

	print("All: {}".format(count_all))
	print("Pos: {}".format(count_pos))
	print("Neg: {}".format(count_neg))
	print("Percentage Pos: {}".format(100 * count_pos / count_all))

def prediction():
	model = keras.models.load_model('model.keras')
	df = pd.read_csv("test_converted.csv")
	prediction = model.predict(df.values)
	print(prediction)


def play_sound():
	def sound(x,z):
	 frequency = x # Our played note will be 440 Hz
	 fs = 44100  # 44100 samples per second
	 seconds = z  # Note duration of 3 seconds

	 # Generate array with seconds*sample_rate steps, ranging between 0 and seconds
	 t = np.linspace(0, seconds, seconds * fs, False)

	 # Generate a 440 Hz sine wave
	 note = np.sin(frequency * t * 2 * np.pi)

	 # Ensure that highest value is in 16-bit range
	 audio = note * (2**15 - 1) / np.max(np.abs(note))
	 # Convert to 16-bit data
	 audio = audio.astype(np.int16)

	 # Start playback
	 play_obj = sa.play_buffer(audio, 1, 2, fs)

	 # Wait for playback to finish before exiting
	 play_obj.wait_done()

	sound(300,2)

	sound(200,1)


if __name__ == '__main__':
	# convert_training_data()
	# convert_test_data()
	main()
	# data_exploration()


