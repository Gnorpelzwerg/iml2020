
import sklearn
from tensorflow import keras
import tensorflow.keras.backend as K
import tensorflow as tf
import pandas as pd
import numpy as np
import math
from tensorflow.keras import layers
from tensorflow.keras import Input
from tensorflow.keras.models import Model
from tensorflow.keras import models
from tensorflow.keras.applications import VGG16
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow.keras.applications.vgg16 import decode_predictions
import os
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, BatchNormalization, Activation, Dropout
from tensorflow.keras.metrics import Accuracy, BinaryAccuracy

RANDOM_SEED = 42
np.random.seed(seed=RANDOM_SEED)
tf.random.set_seed(seed=RANDOM_SEED)

IMG_WIDTH = int(224)
IMG_HEIGHT = int(224)
NUM_COLORS = 3
IMG_PATH = '../heinrich4/food'
IS_MAC = False


def predict_images():
	model = VGG16(include_top=False, input_shape=(IMG_WIDTH,IMG_HEIGHT,NUM_COLORS), pooling='max')
	i = 0
	features = np.empty([10000,512])
	while i < 10000:
		if i % 50 == 0:
			print(i)
		image = load_img(IMG_PATH + '/{:05d}.jpg'.format(i), target_size=(IMG_WIDTH, IMG_HEIGHT))
		image = img_to_array(image)
		image = preprocess_input(image)
		image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
		prediction = model.predict(image)
		features[i] = prediction


		i += 1
	pd.DataFrame(features).to_csv('food_predicted.csv', header=False, index=False)

'''
def show_progress( name, progress, bars = 50 ):
	
	print( name + " " + int(progress*bars) * '█' + int((1-progress)*bars) * '-' + " " + str(int(progress*100)) + "%", end = '\r' )
	
	if progress >= 1:
		
		print( )

def predict_images( ):

	image_width = 224
	image_height = 224
	image_num_channels = 3
	image_path = IMG_PATH
	output_file_name = "food_predicted.csv"

	#load images

	image_names = os.listdir( image_path )
	
	if( IS_MAC ):
		image_names = image_names[ :10 ]

	image_names_short = [ name[ :5 ] for name in image_names ]
	images = [ ]

	for i, name in enumerate( image_names ):

		img = image.load_img( IMG_PATH + "/{}".format( name ), target_size = ( image_width, image_height ))
		images.append( img )
		show_progress( "Reading images", (i + 1)/ len( image_names ))

	# process with vgg16

	show_progress( "Preparing images", 0 )
	X = np.array([ preprocess_input( image.img_to_array( img ) ) for img in images ])
	show_progress( "Preparing images", 1 )
	print( X.shape )
	
	vgg16 = VGG16( weights = "imagenet", include_top = False, input_shape = ( image_width, image_height, image_num_channels ), pooling = "max" )
	show_progress( "Processing images", 0 )
	y = vgg16.predict( X )
	show_progress( "Processing images", 1 )
	print( y.shape )

	show_progress( "Flattening output", 0 )
	y_flat = np.array([ np.ndarray.flatten( y[ i ]) for i in range( y.shape[ 0 ])])
	show_progress( "Flattening output", 1 )
	print( y_flat.shape )
	
	show_progress( "Saving output", 0 )
	pd.DataFrame( y_flat, index = image_names_short ).to_csv( output_file_name, index = True, header = False, float_format = "%.3f" )
	show_progress( "Saving output", 1 )
	print( "preprocess_vgg16: Done" )
'''

def build_datasets():
	food_predicted = pd.read_csv("food_predicted.csv", header=None).to_numpy()
	train_triplets = pd.read_csv("train_triplets.txt", delim_whitespace=True, header=None)
	test_triplets = pd.read_csv("test_triplets.txt", delim_whitespace=True, header=None)

	features = np.empty([train_triplets.shape[0]*2, 3 * 512])
	label = np.empty([train_triplets.shape[0]*2, 1])

	# transform training data

	for index, row in train_triplets.iterrows():
		img_0 = row[0]
		img_1 = row[1]
		img_2 = row[2]

		img_0 = food_predicted[img_0]
		img_1 = food_predicted[img_1]
		img_2 = food_predicted[img_2]
		
		row_1 = np.concatenate([img_0, img_1, img_2])
		row_2 = np.concatenate([img_0, img_2, img_1])
		
		features[index * 2] = row_1
		features[index * 2 + 1] = row_2
		
		label[index * 2] = 1
		label[index * 2 + 1] = 0

	# transform testing data

	data = np.empty([test_triplets.shape[0], 3 * 512])

	for index, row in test_triplets.iterrows():
		img_0 = row[0]
		img_1 = row[1]
		img_2 = row[2]

		img_0 = food_predicted[img_0]
		img_1 = food_predicted[img_1]
		img_2 = food_predicted[img_2]
		
		row_1 = np.concatenate([img_0, img_1, img_2])
		
		data[index] = row_1

	'''
	print('here5')

	pd.DataFrame(features).to_csv('features.csv', header=False, index=False)
	pd.DataFrame(label).to_csv('label.csv', header=False, index=False)

	print('here6')
	'''
	
	return features, label, data

def main():
	X_train, y_train, X_test = build_datasets()
	y_train = keras.utils.to_categorical(y_train)
	print(X_train.shape)
	print(y_train.shape)
	print(X_test.shape)

	# Shuffle
	perm = np.arange(X_test.shape[0])
	np.random.shuffle(perm)
	X_train = X_train[perm]
	y_train = y_train[perm]

	num_neurons = 3 * 512
	activation_1 = 'relu'
	activation_2 = 'relu'
	dropout = 0.2

	metrics = [
		Accuracy(name='acc'),
		BinaryAccuracy(name='bin_acc')
	]

	model = Sequential([
		Dense(num_neurons, input_dim=3*512, kernel_initializer='uniform', activation=activation_1),
		BatchNormalization(),
		Dropout(dropout),

		Dense(num_neurons, input_dim=3*512, kernel_initializer='uniform', activation=activation_2),
		BatchNormalization(),
		Dropout(dropout),

		Dense(2, kernel_initializer='uniform', activation='softmax')
	])

	model.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=metrics)

	hist = model.fit(X_train, y_train, validation_split=0.1, epochs=10)

	accuracy = hist.history['val_acc'][-1]
	binary_accuracy = hist.history['val_bin_acc'][-1]

	print('Accuracy: {}'.format(accuracy))
	print('Binary Accuracy: {}'.format(binary_accuracy))

	y_test = model.predict_classes(X_test)
	pd.DataFrame(y_test).to_csv('result.csv', header=False, index=False)

	y_test = model.predict(X_test)
	pd.DataFrame(y_test).to_csv('result_1.csv', header=False, index=False)

if __name__ == '__main__':
	# predict_images()
	main()
