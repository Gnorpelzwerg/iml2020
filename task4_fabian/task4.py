
import sklearn
from tensorflow import keras
import tensorflow.keras.backend as K
import tensorflow as tf
import pandas as pd
import numpy as np
import math
from tensorflow.keras import layers
from tensorflow.keras import Input
from tensorflow.keras.models import Model
from tensorflow.keras import models
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.callbacks import Callback



from tensorflow.keras.applications import VGG16
#from tensorflow.keras.applications.vgg16 import preprocess_input, decode_predictions

from tensorflow.keras.applications import Xception

import os
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, BatchNormalization, Activation, Dropout
from tensorflow.keras.metrics import Accuracy, BinaryAccuracy, CategoricalAccuracy
from sklearn.model_selection import train_test_split

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

RANDOM_SEED = 42


IMG_WIDTH = 464
IMG_HEIGHT = 324
NUM_COLORS = 3
IMG_PATH = '../heinrich4/food'
IS_MAC = False

IMAGE_FEATURE_SIZE = 512


def predict_images_VGG16():
	if IMAGE_FEATURE_SIZE != 512:
		raise Exception('You must change IMAGE_FEATURE_SIZE to 512')
	model = VGG16(include_top=False, input_shape=(IMG_WIDTH,IMG_HEIGHT,NUM_COLORS), pooling='max')
	i = 0
	features = np.empty([10000,IMAGE_FEATURE_SIZE])
	for i in range(10000):
		if i % 50 == 0:
			print(i)
		image = load_img(IMG_PATH + '/{:05d}.jpg'.format(i), target_size=(IMG_WIDTH, IMG_HEIGHT))
		image = img_to_array(image)
		image = tf.keras.applications.vgg16.preprocess_input(image)
		image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
		prediction = model.predict(image)
		features[i] = prediction

	pd.DataFrame(features).to_csv('food_predicted.csv', header=False, index=False)

def predict_images_Xception():
	if IMAGE_FEATURE_SIZE != 2048:
		raise Exception('You must change IMAGE_FEATURE_SIZE to 2048')

	model = Xception(include_top=False, input_shape=(IMG_WIDTH, IMG_HEIGHT, NUM_COLORS), pooling='max')
	i = 0
	features = np.empty([10000,IMAGE_FEATURE_SIZE])
	for i in range(10000):
		if i % 50 == 0:
			print(i)
		image = load_img(IMG_PATH + '/{:05d}.jpg'.format(i), target_size=(IMG_WIDTH, IMG_HEIGHT))
		image = img_to_array(image)
		image = tf.keras.applications.xception.preprocess_input(image)
		image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
		prediction = model.predict(image)
		features[i] = prediction

	pd.DataFrame(features).to_csv('food_predicted.csv', header=False, index=False)

def get_benchmark_data_as_triplets():
	return pd.read_csv("test_triplets.txt", delim_whitespace=True, header=None).to_numpy().astype(int)

def split_training_data(desired_val_set_size=0.05):

	triplets = pd.read_csv("train_triplets.txt", delim_whitespace=True, header=None).to_numpy()
	np.random.shuffle(triplets)

	# Attempting to build a validation set of desired size

	print('Finding out the right number of tuples to reserve for the validation set...')
	print('target: {}'.format(desired_val_set_size))

	too_few_validation_triplets = True
	portion_to_take_out = 0.0

	while too_few_validation_triplets:

		num_triplets_all = triplets.shape[0]
		num_triplets_val = 0
		num_used_names = 0

		used_names = np.full((10000,1),False)
		used_names_buffer = np.full((10000,1),False)
		used_triplets = np.full((num_triplets_all,1),False)

		while num_triplets_all * portion_to_take_out > num_triplets_val:

			'''
			print('num_triplets_all: {}'.format(num_triplets_all))
			print('num_triplets_val: {}'.format(num_triplets_val))
			print('num_used_names: {}'.format(num_used_names))
			'''

			num_triplets_val_initial = num_triplets_val


			# Add triplets to validation set
			for i in range(num_triplets_all):

				t = triplets[i]

				if used_triplets[i]:
					continue

				if used_names[t[0]] or used_names[t[1]] or used_names[t[2]]:

					num_triplets_val += 1

					used_triplets[i] = True
					
					if not (used_names[t[0]] or used_names_buffer[t[0]]):
						num_used_names += 1

					if not (used_names[t[1]] or used_names_buffer[t[1]]):
						num_used_names += 1

					if not (used_names[t[2]] or used_names_buffer[t[2]]):
						num_used_names += 1

					used_names_buffer[t[0]] = True
					used_names_buffer[t[1]] = True
					used_names_buffer[t[2]] = True

				# We have enough triplets
				if num_triplets_all * portion_to_take_out <= num_triplets_val:
					break

			used_names = np.logical_or(used_names, used_names_buffer)
			used_names_buffer = np.full((10000,1),False)

			# We have enough triplets
			if num_triplets_all * portion_to_take_out <= num_triplets_val:
				break

			# Add another name to the triplet name set because nothing was added but something needs to be added
			if num_triplets_val_initial == num_triplets_val:

				go = True

				while go:
					random_number = np.random.randint(10000)
					if not used_names[random_number]:
						used_names[random_number] = True

						num_used_names += 1

						go = False

		'''
		print('done')
		print('num_triplets_all: {}'.format(num_triplets_all))
		print('num_triplets_val: {}'.format(num_triplets_val))
		print('num_used_names: {}'.format(num_used_names))
		'''

		num_triplets_train = 0
		num_triplets_dirty = 0

		# counting 
		for i in range(num_triplets_all):
			
			t = triplets[i]

			if used_triplets[i]:
				continue
			elif used_names[t[0]] or used_names[t[1]] or used_names[t[2]]:
				num_triplets_dirty += 1
			else:
				num_triplets_train += 1

		portion_validation = num_triplets_val / float(num_triplets_train)


		if portion_validation > desired_val_set_size:
			too_few_validation_triplets = False
			print('Found the right number!')
			print('num_triplets_all: {}'.format(num_triplets_all))
			print('num_triplets_train: {}'.format(num_triplets_train))
			print('num_triplets_dirty: {}'.format(num_triplets_dirty))
			print('num_triplets_val: {}'.format(num_triplets_val))
			print('num_used_names: {}'.format(num_used_names))
			print('actual validation set size: {}'.format(portion_validation))
		else:
			portion_to_take_out += 0.001

	# extracting arrays
	print('extracting arrays...')
	triplets_train = np.empty((num_triplets_train,3))
	i_train = 0
	triplets_val = np.empty((num_triplets_val,3))
	i_val = 0

	for i in range(num_triplets_all):
	
		t = triplets[i]

		if used_triplets[i]:
			triplets_val[i_val] = t
			i_val += 1
		elif not (used_names[t[0]] or used_names[t[1]] or used_names[t[2]]):
			triplets_train[i_train] = t
			i_train += 1
	
	print('done!')
	return triplets_train.astype(int), triplets_val.astype(int)

def build_dataset_from_triplets(triplets, is_benchmark=False):
	print('building dataset from triplets...')
	print(triplets.shape)

	length = triplets.shape[0]

	food_predicted = pd.read_csv("food_predicted.csv", header=None).to_numpy()

	if is_benchmark:
		ds = np.empty([length, 3 * IMAGE_FEATURE_SIZE])
	else:
		ds = np.empty([length * 2, 3 * IMAGE_FEATURE_SIZE])	
	

	for i in range(length):
		t = triplets[i]

		#print(t)

		img_0 = food_predicted[t[0]]
		img_1 = food_predicted[t[1]]
		img_2 = food_predicted[t[2]]

		if is_benchmark:
			ds[i] = np.concatenate([img_0, img_1, img_2])			
		else:
			ds[i * 2] = np.concatenate([img_0, img_1, img_2])	
			ds[i * 2 + 1] = np.concatenate([img_0, img_2, img_1])

	print('done')
	return ds

def build_labels(length):
	print('building labels...')
	l = np.full([length,1],0)
	for i in range(len(l)):
		if i % 2 == 0:
			l[i] = 1
	print('done')
	return l

def compute_own_accuracy(truth, prediction):
	similar_positions = 0
	length = len(truth)
	for i in range(length):
		if prediction[i] == truth[i][1]:
			similar_positions += 1
	return float(similar_positions)/float(length)

class EarlyStoppingByLossVal(Callback):
    def __init__(self, monitor='val_acc', value=0.001, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn("Early stopping requires %s available!" % self.monitor, RuntimeWarning)

        if current > self.value:
            if self.verbose > 0:
                print("Epoch %05d: early stopping THR" % epoch)
            self.model.stop_training = True

def main():

	# Configure run
	start_at = 2
	only_do_one_training = True
	fit_on_all_data = True
	desired_acc = 0.71

	print('\n\n\n\n\n\nstarting at config nr. {}, only_do_one_training={}, fit_on_all_data={}, desired_acc={}\n\n\n\n\n\n'.format(start_at, only_do_one_training, fit_on_all_data, desired_acc))

	print('manipulating data...')

	if fit_on_all_data:
		triplets, _ = split_training_data(0)
		X = build_dataset_from_triplets(triplets)
		y = build_labels(len(X))


	triplets_train, triplets_val = split_training_data()
	triplets_benchmark = get_benchmark_data_as_triplets()

	X_train = build_dataset_from_triplets(triplets_train)
	X_val = build_dataset_from_triplets(triplets_val)
	X_benchmark = build_dataset_from_triplets(triplets_benchmark, is_benchmark=True)

	y_train = build_labels(len(X_train))
	y_val = build_labels(len(X_val))

	# we need this if we have two outputs
	y_train = keras.utils.to_categorical(y_train)
	y_val = keras.utils.to_categorical(y_val)

	print('building model...')

	try_num_neurons = [50]
	try_l = [0.001]
	try_dropout = [0.35]
	try_num_layers = [3]
	activation = ['tanh','softsign','softplus', 'relu', 'sigmoid', 'exponential']

	# Tried until config nr 869. 362 was best

	# values are from previous partial runs
	best_acc = 0
	best_description = ''

	config_nr = 0

	metrics = [
		CategoricalAccuracy(name='acc')
		#Accuracy(name='acc')
	]

	callbacks = [
		EarlyStopping(monitor='val_loss', patience=20), 
		EarlyStoppingByLossVal(monitor='val_acc', value=desired_acc, verbose=1)
	]
	for act in activation:
		for num_layers in try_num_layers:
			for num_neurons in try_num_neurons:
				for l in try_l:
					for dropout in try_dropout:

						config_nr += 1

						if config_nr < start_at:
							continue

						if only_do_one_training and config_nr > start_at:
							continue

						description = 'config: {}, activation: {}, hidden layers: {}, neurons: {}, l: {}, dropout: {}'.format(config_nr, act, num_layers, num_neurons, l, dropout)

						print('\n\n\n')
						print(description)

						regularizer = regularizers.l2(l = l)

						model = Sequential()

						for i in range(num_layers):

							model.add(BatchNormalization())

							if i == 0:
								model.add(Dense(
									num_neurons, 
									input_dim=3*IMAGE_FEATURE_SIZE, 
									kernel_initializer='uniform', 
									activation=act,
									kernel_regularizer = regularizer
								))	
							else:
								model.add(Dense(
									num_neurons, 
									kernel_initializer='uniform', 
									activation=act,
									kernel_regularizer = regularizer
								))	

							model.add(Dropout(dropout))

						model.add(Dense(2, kernel_initializer='uniform', activation='softmax'))

						model.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=metrics)

						model_all = model

						print('training model...')
						hist = model.fit(X_train, y_train, validation_data=(X_val, y_val), epochs=10)
						hist = model.fit(X_train, y_train, validation_data=(X_val, y_val), epochs=200, callbacks=callbacks)

						n_epochs = len(hist.history['loss'])

						# Output Keras metrics
						acc = hist.history['val_acc'][-1]
						print('Accuracy: {}'.format(acc))

						if acc > best_acc or acc >= desired_acc:
							print('********** new best (or very good) acc: {} **********'.format(acc))
							print('this was config nr {}'.format(config_nr))
							print(description)

							best_acc = acc
							best_description = description

						else:
							print('---------- no record ----------')
							print('best until now:' )
							print(best_acc)
							print(best_description)

						if fit_on_all_data:
							print('Training on all data')
							hist = model_all.fit(X_train, y_train, epochs=n_epochs)

							print('predicting benchmark triplets using all data...') 
							# Output categorical values
							y_benchmark = model_all.predict_classes(X_benchmark, batch_size=32)
						else:
							print('predicting benchmark triplets using only training data...') 
							# Output categorical values
							y_benchmark = model.predict_classes(X_benchmark, batch_size=32)

						print('Writing data')	
						pd.DataFrame(y_benchmark).to_csv('result_acc{}_config{}.txt'.format(acc, config_nr), header=False, index=False)

						print('done')

if __name__ == '__main__':
	# split_training_data()
	# predict_images_Xception()
	# predict_images_VGG16()
	main() 





























'''
def show_progress( name, progress, bars = 50 ):
	
	print( name + " " + int(progress*bars) * '█' + int((1-progress)*bars) * '-' + " " + str(int(progress*100)) + "%", end = '\r' )
	
	if progress >= 1:
		
		print( )

def predict_images( ):

	image_width = 224
	image_height = 224
	image_num_channels = 3
	image_path = IMG_PATH
	output_file_name = "food_predicted.csv"

	#load images

	image_names = os.listdir( image_path )
	
	if( IS_MAC ):
		image_names = image_names[ :10 ]

	image_names_short = [ name[ :5 ] for name in image_names ]
	images = [ ]

	for i, name in enumerate( image_names ):

		img = image.load_img( IMG_PATH + "/{}".format( name ), target_size = ( image_width, image_height ))
		images.append( img )
		show_progress( "Reading images", (i + 1)/ len( image_names ))

	# process with vgg16

	show_progress( "Preparing images", 0 )
	X = np.array([ preprocess_input( image.img_to_array( img ) ) for img in images ])
	show_progress( "Preparing images", 1 )
	print( X.shape )
	
	vgg16 = VGG16( weights = "imagenet", include_top = False, input_shape = ( image_width, image_height, image_num_channels ), pooling = "max" )
	show_progress( "Processing images", 0 )
	y = vgg16.predict( X )
	show_progress( "Processing images", 1 )
	print( y.shape )

	show_progress( "Flattening output", 0 )
	y_flat = np.array([ np.ndarray.flatten( y[ i ]) for i in range( y.shape[ 0 ])])
	show_progress( "Flattening output", 1 )
	print( y_flat.shape )
	
	show_progress( "Saving output", 0 )
	pd.DataFrame( y_flat, index = image_names_short ).to_csv( output_file_name, index = True, header = False, float_format = "%.3f" )
	show_progress( "Saving output", 1 )
	print( "preprocess_vgg16: Done" )
'''

'''
def build_datasets():

	print('building datasets...')

	food_predicted = pd.read_csv("food_predicted.csv", header=None).to_numpy()
	train_triplets = pd.read_csv("train_triplets.txt", delim_whitespace=True, header=None)
	test_triplets = pd.read_csv("test_triplets.txt", delim_whitespace=True, header=None)

	features = np.empty([train_triplets.shape[0]*2, 3 * IMAGE_FEATURE_SIZE])
	label = np.empty([train_triplets.shape[0]*2, 1])

	# transform training data

	for index, row in train_triplets.iterrows():
		img_0 = row[0]
		img_1 = row[1]
		img_2 = row[2]

		img_0 = food_predicted[img_0]
		img_1 = food_predicted[img_1]
		img_2 = food_predicted[img_2]
		
		row_1 = np.concatenate([img_0, img_1, img_2])
		row_2 = np.concatenate([img_0, img_2, img_1])
		
		features[index * 2] = row_1
		features[index * 2 + 1] = row_2
		
		label[index * 2] = 1
		label[index * 2 + 1] = 0

	# transform testing data

	data = np.empty([test_triplets.shape[0], 3 * IMAGE_FEATURE_SIZE])

	for index, row in test_triplets.iterrows():
		img_0 = row[0]
		img_1 = row[1]
		img_2 = row[2]

		img_0 = food_predicted[img_0]
		img_1 = food_predicted[img_1]
		img_2 = food_predicted[img_2]
		
		row_1 = np.concatenate([img_0, img_1, img_2])
		
		data[index] = row_1

	
	print('here5')

	pd.DataFrame(features).to_csv('features.csv', header=False, index=False)
	pd.DataFrame(label).to_csv('label.csv', header=False, index=False)

	print('here6')
	
	
	return features, label, data
'''