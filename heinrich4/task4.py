import numpy as np
import os
import sys
import pandas as pd
from tensorflow import keras as keras
from tensorflow.keras.applications import VGG16
from tensorflow.keras.preprocessing import image

IS_SERVER = False
IS_MAC = True

def handle_args( ):

	global IS_SERVER
	global IS_MAC

	if len( sys.argv ) >= 2:
		if sys.argv[ 1 ] == "server":
			IS_SERVER = True
			IS_MAC = False

	if IS_MAC:
		print( "Warning: running code in development environment" )

def show_progress( name, progress, bars = 50 ):
	
	print( name + " " + int(progress*bars) * '█' + int((1-progress)*bars) * '-' + " " + str(int(progress*100)) + "%", end = '\r' )
	
	if progress >= 1:
		
		print( )

def preprocess_vgg16( ):

	image_width = 224
	image_height = 224
	image_num_channels = 3

	#load images

	image_names = os.listdir( "food" )
	
	if( IS_MAC ):
		image_names = image_names[ :10 ]

	image_names_short = [ name[ :5 ] for name in image_names ]
	images = [ ]

	for i, name in enumerate( image_names ):

		img = image.load_img( "food/{}".format( name ), target_size = ( image_width, image_height ))
		images.append( img )
		show_progress( "Reading images", (i + 1)/ len( image_names ))

	# process with vgg16

	show_progress( "Preparing images", 0 )
	X = np.array([ image.img_to_array( img ) for img in images ])
	show_progress( "Preparing images", 1 )
	print( X.shape )
	
	vgg16 = VGG16( weights = "imagenet", include_top = False, input_shape = ( image_width, image_height, image_num_channels ), pooling = "max" )
	show_progress( "Processing images", 0 )
	y = vgg16.predict( X )
	show_progress( "Processing images", 1 )
	print( y.shape )

	show_progress( "Flattening output", 0 )
	y_flat = np.array([ np.ndarray.flatten( y[ i ]) for i in range( y.shape[ 0 ])])
	show_progress( "Flattening output", 1 )
	print( y_flat.shape )
	
	show_progress( "Saving output", 0 )
	pd.DataFrame( y_flat, index = image_names_short ).to_csv( "images_vgg16_processed.csv", index = True, header = False, float_format = "%.3f" )
	show_progress( "Saving output", 1 )
	print( "preprocess_vgg16: Done" )

#for task4_fabian/task4.py
def predict_images( ):

	image_width = 224
	image_height = 224
	image_num_channels = 3
	image_path = IMG_PATH
	output_file_name = "food_predicted.csv"

	#load images

	image_names = os.listdir( image_path )
	
	if( IS_MAC ):
		image_names = image_names[ :10 ]

	image_names_short = [ name[ :5 ] for name in image_names ]
	images = [ ]

	for i, name in enumerate( image_names ):

		img = image.load_img( "food/{}".format( name ), target_size = ( image_width, image_height ))
		images.append( img )
		show_progress( "Reading images", (i + 1)/ len( image_names ))

	# process with vgg16

	show_progress( "Preparing images", 0 )
	X = np.array([ image.img_to_array( img ) for img in images ])
	show_progress( "Preparing images", 1 )
	print( X.shape )
	
	vgg16 = VGG16( weights = "imagenet", include_top = False, input_shape = ( image_width, image_height, image_num_channels ), pooling = "max" )
	show_progress( "Processing images", 0 )
	y = vgg16.predict( X )
	show_progress( "Processing images", 1 )
	print( y.shape )

	show_progress( "Flattening output", 0 )
	y_flat = np.array([ np.ndarray.flatten( y[ i ]) for i in range( y.shape[ 0 ])])
	show_progress( "Flattening output", 1 )
	print( y_flat.shape )
	
	show_progress( "Saving output", 0 )
	pd.DataFrame( y_flat, index = image_names_short ).to_csv( output_file_name, index = True, header = False, float_format = "%.3f" )
	show_progress( "Saving output", 1 )
	print( "preprocess_vgg16: Done" )

if __name__ == "__main__" :
	
	handle_args( )
	preprocess_vgg16( )
